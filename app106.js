// rest parameter
function show(...args) {
    let sum = 0;
    for (const i of args) {
        sum = sum + i;
    }
    return sum;
}
const result = show(10, 20, 30, 40, 50, 60, 70, 80);
console.log(result)